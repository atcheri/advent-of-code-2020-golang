package handheld

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPlayer_Execute(t *testing.T) {
	t.Run("End with the field Accumulator equal to 1", func(t *testing.T) {
		player := Player{Instructions: []Instruction{{ID: 1, Command: Acc, Steps: 1}}, Accumulator: 0}
		player.Execute()
		assert.Equal(t, 1, player.Accumulator)
	})
	t.Run("End with the field Accumulator equal to 1", func(t *testing.T) {
		player := Player{Instructions: []Instruction{
			{ID: 1, Command: Jmp, Steps: 1},
			{ID: 2, Command: Acc, Steps: 1},
		}, Accumulator: 0}
		player.Execute()
		assert.Equal(t, 1, player.Accumulator)
	})
	t.Run("End with the field Accumulator equal to 1 again", func(t *testing.T) {
		player := Player{Instructions: []Instruction{
			{ID: 1, Command: Nop, Steps: 2},
			{ID: 2, Command: Acc, Steps: 1},
			{ID: 3, Command: Jmp, Steps: 1},
		}, Accumulator: 0}
		player.Execute()
		assert.Equal(t, 1, player.Accumulator)
	})
}

func TestPlayer_Execute_From_Sample(t *testing.T) {
	t.Run("Given a list of instructions", func(t *testing.T) {
		instructions := []Instruction{
			{ID: 1, Command: Nop, Steps: 0},
			{ID: 2, Command: Acc, Steps: 1},
			{ID: 3, Command: Jmp, Steps: 4},
			{ID: 4, Command: Acc, Steps: 3},
			{ID: 5, Command: Jmp, Steps: -3},
			{ID: 6, Command: Acc, Steps: -99},
			{ID: 7, Command: Acc, Steps: 1},
			{ID: 8, Command: Jmp, Steps: -4},
			{ID: 9, Command: Acc, Steps: 6},
		}
		t.Run("End with the field Accumulator equal to 5", func(t *testing.T) {
			player := Player{Instructions: instructions, Accumulator: 0}
			player.Execute()
			assert.Equal(t, 5, player.Accumulator)
		})
	})
}

func TestPlayer_CorretAndExecute_From_Sample(t *testing.T) {
	t.Run("Given a list of instructions ending with Nop", func(t *testing.T) {
		instructions := []Instruction{
			{ID: 1, Command: Nop, Steps: 0},
			{ID: 2, Command: Acc, Steps: 1},
			{ID: 3, Command: Jmp, Steps: -2},
			{ID: 4, Command: Nop, Steps: 0},
		}
		t.Run("End with the field Accumulator equal to 1", func(t *testing.T) {
			player := Player{Instructions: instructions, Accumulator: 0}
			acc, finished := player.CorretAndExecute()
			assert.Equal(t, 1, acc)
			assert.True(t, finished)
		})
	})
	t.Run("Given a list of instructions ending with Nop", func(t *testing.T) {
		instructions := []Instruction{
			{ID: 1, Command: Nop, Steps: 0},
			{ID: 2, Command: Acc, Steps: 1},
			{ID: 3, Command: Jmp, Steps: -2},
			{ID: 4, Command: Nop, Steps: 0},
		}
		t.Run("End with the field Accumulator equal to 1", func(t *testing.T) {
			player := Player{Instructions: instructions, Accumulator: 0}
			acc, finished := player.CorretAndExecute()
			assert.Equal(t, 1, acc)
			assert.True(t, finished)
		})
	})
	t.Run("Given a list of instructions ending with Acc +6", func(t *testing.T) {
		instructions := []Instruction{
			{ID: 1, Command: Nop, Steps: 0},
			{ID: 2, Command: Acc, Steps: 1},
			{ID: 3, Command: Jmp, Steps: -2},
			{ID: 4, Command: Acc, Steps: 6},
		}
		t.Run("End with the field Accumulator equal to 7", func(t *testing.T) {
			player := Player{Instructions: instructions, Accumulator: 0}
			acc, finished := player.CorretAndExecute()
			assert.Equal(t, 7, acc)
			assert.True(t, finished)
		})
	})
	t.Run("Given a list of instructions from the sample", func(t *testing.T) {
		instructions := []Instruction{
			{ID: 1, Command: Nop, Steps: 0},
			{ID: 2, Command: Acc, Steps: 1},
			{ID: 3, Command: Jmp, Steps: 4},
			{ID: 4, Command: Acc, Steps: 3},
			{ID: 5, Command: Jmp, Steps: -3},
			{ID: 6, Command: Acc, Steps: -99},
			{ID: 7, Command: Acc, Steps: 1},
			{ID: 8, Command: Jmp, Steps: -4},
			{ID: 9, Command: Acc, Steps: 6},
		}
		t.Run("End with the field Accumulator equal to 8", func(t *testing.T) {
			player := Player{Instructions: instructions, Accumulator: 0}
			acc, finished := player.CorretAndExecute()
			assert.Equal(t, 8, acc)
			assert.True(t, finished)
		})
	})
}
