package handheld

import "errors"

// Player is a player with instructions
type Player struct {
	Instructions []Instruction
	Accumulator  int
}

// Execute all the instructions
func (p *Player) Execute() {

	stream := p.streamAcc()

	for acc := range stream {
		p.Accumulator += acc
	}
}

func (p *Player) streamAcc() chan int {
	visited := make(map[int]interface{})
	curr := p.Instructions[0]
	remaining := len(p.Instructions)
	length := remaining
	intStream := make(chan int)

	go func() {
		defer close(intStream)
		for {
			if _, ok := visited[curr.ID]; ok {
				break
			}
			visited[curr.ID] = struct{}{}

			nextIndex := curr.ID // we don't add 1 because it starts at 1
			switch curr.Command {
			case Acc:
				// p.Accumulator += curr.Steps
				intStream <- curr.Steps
			case Jmp:
				nextIndex += curr.Steps - 1
			case Nop:
			}

			curr = p.Instructions[nextIndex]

			if nextIndex >= length {
				break
			}

			remaining--
			if remaining == 0 {
				break
			}
		}
	}()
	return intStream
}

func switchInstruction(i Instruction) Instruction {
	switch i.Command {
	case Jmp:
		return Instruction{ID: i.ID, Command: Nop, Steps: i.Steps}
	case Nop:
		return Instruction{ID: i.ID, Command: Jmp, Steps: i.Steps}
	default:
		return i
	}
}

// CorretAndExecute self corrects it's list of instructions and execute til the end
func (p *Player) CorretAndExecute() (acc int, finished bool) {
	modifiedInstructions := make([]Instruction, len(p.Instructions))
	for i, ins := range p.Instructions {
		if ins.Command == Acc {
			continue
		}
		copy(modifiedInstructions, p.Instructions)
		switchedInstruction := switchInstruction(ins)
		modifiedInstructions[i] = switchedInstruction
		acc, finished = ExecuteInstructions(modifiedInstructions)
		if finished {
			return acc, finished
		}
	}
	return acc, false
}

// ExecuteInstructions is
func ExecuteInstructions(instructions []Instruction) (acc int, finished bool) {
	curr := instructions[0]
	if curr.Command == Jmp && curr.Steps == 0 {
		return 0, false
	}
	visited := make(map[int]interface{})
	remaining := len(instructions)
	length := remaining
	var err error
	var ins *Instruction
	for {
		if _, ok := visited[curr.ID]; ok {
			return acc, false
		}
		visited[curr.ID] = struct{}{}

		nextID := curr.ID + 1
		switch curr.Command {
		case Acc:
			acc += curr.Steps
		case Jmp:
			nextID = curr.ID + curr.Steps
		case Nop:
		}

		if nextID > length {
			break
		}

		var findInstruction = func(id int) (*Instruction, error) {
			for _, ins := range instructions {
				if ins.ID == id {
					return &ins, nil
				}
			}
			return nil, errors.New("Instruction not found error")
		}
		ins, err = findInstruction(nextID)
		if err != nil {
			panic(err.Error())
		}
		curr = *ins

		remaining--
		if remaining == 0 {
			return acc, false
		}
	}

	return acc, true
}
