package handheld

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewInstruction_Error(t *testing.T) {
	t.Run("Given an empty instruction", func(t *testing.T) {
		t.Run("Returns nil", func(t *testing.T) {
			instruction := NewInstruction(0, "")
			assert.Nil(t, instruction)
			// assert.EqualError(t, err, emptyParamErrorMsg)
		})
	})
	t.Run("Given an invalid instruction", func(t *testing.T) {
		t.Run("Returns nil", func(t *testing.T) {
			instruction := NewInstruction(0, "qqqqqqqqqqthtqa")
			assert.Nil(t, instruction)
		})
	})
}

func TestNewInstruction_No_Error(t *testing.T) {
	t.Run("Given the instruction \"acc +1\"", func(t *testing.T) {
		t.Run("Returns a valid Instruction", func(t *testing.T) {
			instruction := NewInstruction(1, "acc +1")
			assert.NotNil(t, instruction)
			assert.EqualValues(t, &Instruction{ID: 1, Command: Acc, Steps: 1}, instruction)
		})
	})
	t.Run("Given the instruction \"jmp -4\"", func(t *testing.T) {
		t.Run("Returns a valid Instruction", func(t *testing.T) {
			instruction := NewInstruction(1, "jmp -4")
			assert.NotNil(t, instruction)
			assert.EqualValues(t, &Instruction{ID: 1, Command: Jmp, Steps: -4}, instruction)
		})
	})
}
