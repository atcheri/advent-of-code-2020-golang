package handheld

import (
	"errors"
	"strconv"
	"strings"
)

// Command is
type Command int

const (
	// Acc stands for accumulate
	Acc Command = iota
	// Jmp stands for jump
	Jmp
	// Nop stands for no operation
	Nop
)

var (
	emptyParamErrorMsg   = "Empty parameter error"
	invalidParamErrorMsg = "Invalid parameter error"
)

func (s Command) String() string {
	return [...]string{"acc", "jmp", "nop"}[s]
}

// Instruction has the command and the steps
type Instruction struct {
	ID      int
	Command Command
	Steps   int
}

// NewInstruction is the Instruction factory
func NewInstruction(id int, entry string) *Instruction {
	if entry == "" {
		return nil
	}
	command, steps, err := parse(entry)
	if err != nil {
		return nil
	}

	return &Instruction{ID: id, Command: command, Steps: steps}
}

func parse(entry string) (Command, int, error) {
	parts := strings.Split(entry, " ")
	var command Command
	switch parts[0] {
	case "acc":
		command = Acc
	case "jmp":
		command = Jmp
	case "nop":
		command = Nop
	}

	steps, err := strconv.Atoi(parts[len(parts)-1])
	if err != nil {
		return 0, 0, errors.New(invalidParamErrorMsg)
	}
	return command, steps, nil
}
