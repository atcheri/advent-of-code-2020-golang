package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"gitlab.com/atcheri/advent-of-code-2020-golang/day8/handheld"
)

func readFile(fname string) ([]string, error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	entries := make([]string, len(lines)-1)

	for i, l := range lines {
		if len(l) == 0 {
			continue
		}
		entries[i] = l
	}

	return entries, nil
}

func main() {
	entries, err := readFile("./input")
	if err != nil {
		panic("Coundl't read from input file")
	}

	instructions := make([]handheld.Instruction, len(entries))
	var ins *handheld.Instruction
	for i, e := range entries {
		ins = handheld.NewInstruction(i+1, e)
		if ins == nil {
			log.Printf("Nil encounted at %d with %s", i, e)
			continue
		}
		instructions[i] = *ins
	}

	player := handheld.Player{Instructions: instructions, Accumulator: 0}
	player.Execute()
	fmt.Printf("The cumulated value until reaching the infinite loop is: %d\n", player.Accumulator)

	acc, finished := player.CorretAndExecute()
	if !finished {
		log.Panic("Couldn't finish the game")
	}
	fmt.Printf("The cumulated value once the game is finished is : %d\n", acc)
}
