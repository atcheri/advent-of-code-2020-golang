package findtwoentries

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindIndexes(t *testing.T) {
	t.Run("[ERROR]", func(t *testing.T) {
		t.Run("Empty entries", func(t *testing.T) {
			_, err := findIndexes([]int{}, 0, 2)
			assert.EqualError(t, err, emptyParametersErrorMsg)
		})
		t.Run("One entry", func(t *testing.T) {
			_, err := findIndexes([]int{1}, 0, 2)
			assert.EqualError(t, err, singleParametersErrorMsg)
		})
		t.Run("Target was not found", func(t *testing.T) {
			indexes, err := findIndexes([]int{1, 2}, 0, 2)
			assert.EqualError(t, err, targetNotFoundErrorMsg)
			assert.EqualValues(t, []int{-1, -1}, indexes)
		})
	})
	t.Run("[NO ERROR]", func(t *testing.T) {
		t.Run("Was found for entries = [1, 2] and target = 3", func(t *testing.T) {
			indexes, err := findIndexes([]int{1, 2}, 3, 2)
			assert.Nil(t, err)
			assert.EqualValues(t, []int{0, 1}, indexes)
		})

		t.Run("Was found for entries = [1721, 979, 366, 299, 675, 1456] and target = 2020", func(t *testing.T) {
			indexes, err := findIndexes([]int{1721, 979, 366, 299, 675, 1456}, 2020, 2)
			assert.Nil(t, err)
			assert.EqualValues(t, []int{0, 3}, indexes)
		})

		// 	// t.Run("Was found for entries = [1721, 979, 366, 299, 675, 1456] and target = 2020", func(t *testing.T) {
		// 	// 	indexes, err := findIndexes([]int{1721, 979, 366, 299, 675, 1456}, 2020, 2)
		// 	// 	assert.Nil(t, err)
		// 	// 	assert.Equal(t, 0, i)
		// 	// 	assert.Equal(t, 3, j)
		// 	// })

	})
}
