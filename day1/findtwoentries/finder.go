package findtwoentries

import (
	"fmt"
)

type parametersError struct {
	msg string
}

func (err parametersError) Error() string {
	return err.msg
}

type indexValue struct {
	ind int
	val int
}

var (
	emptyParametersErrorMsg  = "Empty slice parameter error"
	singleParametersErrorMsg = "Single element slice parameter error"
	targetNotFoundErrorMsg   = "Target not found error"
)

func recursiveFind(entries []int, parents []indexValue, depth int, remainingSteps int, target int) ([]int, error) {
	for i, f := range entries {
		realIndex := i + depth - remainingSteps - 1
		if remainingSteps > 0 {
			parents = append(parents, indexValue{ind: realIndex, val: f})
			return recursiveFind(entries[i+1:], parents, depth, remainingSteps-1, target)
		}
		if remainingSteps == 0 {
			var prevSum = 0
			var indexes []int
			// indexes := make([]int, len(parents))
			// copy(indexes, parents)
			for _, iv := range parents {
				indexes = append(indexes, iv.ind)
				prevSum += iv.val
			}
			if prevSum+f == target {
				indexes = append(indexes, realIndex)
				return indexes, nil
			}
			// parents = parents[:0]
			// return nil, parametersError{msg: "Index not found error"}
		}
	}
	return nil, parametersError{msg: targetNotFoundErrorMsg}
}

func findIndexes(entries []int, target int, depth int) ([]int, error) {
	if len(entries) == 0 {
		return []int{0, 0}, parametersError{msg: emptyParametersErrorMsg}
	}
	if len(entries) == 1 {
		return []int{0, 0}, parametersError{msg: singleParametersErrorMsg}
	}
	if len(entries) == 2 && depth == 3 {
		return []int{0, 0}, parametersError{msg: singleParametersErrorMsg}
	}

	indexes, err := recursiveFind(entries, []indexValue{}, depth, depth-1, target)
	if err != nil {
		return []int{-1, -1}, parametersError{msg: targetNotFoundErrorMsg}
	}
	return indexes, nil

}

// Product calculate the product of the found 2 entries when summed equal the target
func Product(entries []int, target int, depth int) int {
	indexes, err := findIndexes(entries, target, depth)
	fmt.Printf("%v, %v", err, indexes)
	if err != nil {
		return 0
	}
	var prod = 1
	for _, i := range indexes {
		prod *= i
	}
	return prod
}
