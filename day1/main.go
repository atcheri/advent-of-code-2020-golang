package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"gitlab.com/atcheri/advent-of-code-2020-golang/day1/findtwoentries"
)

func readFile(fname string) (nums []int, err error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	nums = make([]int, 0, len(lines))

	for _, l := range lines {
		if len(l) == 0 {
			continue
		}
		n, err := strconv.Atoi(l)
		if err != nil {
			return nil, err
		}
		nums = append(nums, n)
	}

	return nums, nil
}

func main() {
	entries, err := readFile("./input")
	if err != nil {
		panic("Coundl't read from input file")
	}
	var target = 2020
	solution1 := findtwoentries.Product(entries, target, 2)
	fmt.Println(solution1)
	solution2 := findtwoentries.Product(entries, target, 3)
	fmt.Println(solution2)
}
