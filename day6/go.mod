module gitlab.com/atcheri/advent-of-code-2020-golang/day6

go 1.15

require (
	github.com/adam-lavrik/go-imath v0.0.0-20200104135348-19d2b923d06c
	github.com/juliangruber/go-intersect v1.0.0
	github.com/stretchr/testify v1.6.1
	gopkg.in/vmarkovtsev/go-lcss.v1 v1.0.0-20181020221121-dfc501d07ea0
)
