package yesno

import (
	"errors"
	"regexp"

	"github.com/juliangruber/go-intersect"

	"gopkg.in/vmarkovtsev/go-lcss.v1"
)

var (
	lowerCaseALphabetRegexp     = "[^a-z]"
	invalidRegexpErrorMsg       = "Invalid regexp (string parameter) error"
	invalidAnswersParamErrorMsg = "Invalid answers param error"
	noAnswersParamErrorMsg      = "No answers param error"
)

// DistinctAnswers parses the given answer string and returns the disctinct provided answers or an error
func DistinctAnswers(answers string) (map[rune]interface{}, error) {
	if answers == "" {
		return nil, errors.New(noAnswersParamErrorMsg)
	}

	// checking the answers with a regex
	re, err := regexp.Compile(lowerCaseALphabetRegexp)
	if err != nil {
		return nil, errors.New(invalidRegexpErrorMsg)
	}
	invalid := re.Match([]byte(answers))
	if invalid {
		return nil, errors.New(invalidAnswersParamErrorMsg)
	}

	distinctAnswers := make(map[rune]interface{})
	var i interface{}
	for _, c := range answers {
		distinctAnswers[c] = i
	}
	return distinctAnswers, nil

}

// LongestCommonAnswers count the longest common answers given by all the group memebers
func LongestCommonAnswers(groupAnswers []string) int {
	if len(groupAnswers) == 1 {
		return len(groupAnswers[0])
	}
	c := lcss.LongestCommonSubstring(func(gas []string) [][]byte {
		byteAnswers := make([][]byte, len(gas))
		for i, ga := range gas {
			byteAnswers[i] = []byte(ga)
		}
		return byteAnswers

	}(groupAnswers)...)
	return len(c)
}

// CommonAnswers count the common answers given by all the group memebers
func CommonAnswers(groupAnswers []string) int {
	if len(groupAnswers) == 1 {
		return len(groupAnswers[0])
	}
	previousAnswers := groupAnswers[0]
	min := len(groupAnswers[0])
	for _, answers := range groupAnswers[1:] {

		c := intersect.Simple(previousAnswers, answers)
		if len(c.([]interface{})) < min {
			min = len(c.([]interface{}))
		}
		previousAnswers = answers

	}
	return min
}
