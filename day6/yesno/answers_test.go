package yesno

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDiscintcAnswers_Error(t *testing.T) {
	t.Run("Given an empty string", func(t *testing.T) {
		t.Run("Returns an error", func(t *testing.T) {
			_, err := DistinctAnswers("")
			assert.NotNil(t, err)
			assert.EqualError(t, err, noAnswersParamErrorMsg)
		})
	})

	t.Run("Given invalid answers ", func(t *testing.T) {
		t.Run("Returns an error for a number", func(t *testing.T) {
			_, err := DistinctAnswers("4")
			assert.NotNil(t, err)
			assert.EqualError(t, err, invalidAnswersParamErrorMsg)
		})
		t.Run("Returns an error for a special character", func(t *testing.T) {
			_, err := DistinctAnswers("$")
			assert.NotNil(t, err)
			assert.EqualError(t, err, invalidAnswersParamErrorMsg)
		})
	})

	t.Run("Given valid and invalid answers", func(t *testing.T) {
		t.Run("Returns an error for 5bc", func(t *testing.T) {
			_, err := DistinctAnswers("5bc")
			assert.NotNil(t, err)
			assert.EqualError(t, err, invalidAnswersParamErrorMsg)
		})
		t.Run("Returns an error for c$b^te", func(t *testing.T) {
			_, err := DistinctAnswers("c$b^te")
			assert.NotNil(t, err)
			assert.EqualError(t, err, invalidAnswersParamErrorMsg)
		})
	})
}

func TestDiscintcAnswers_No_Error(t *testing.T) {
	t.Run("finds 3 disctinct answers for \"abc\"", func(t *testing.T) {
		list, err := DistinctAnswers("abc")
		assert.Nil(t, err)
		assert.Equal(t, 3, len(list))
		_, ok := list[rune("a"[0])]
		assert.True(t, ok)
		_, ok = list[rune("b"[0])]
		assert.True(t, ok)
		_, ok = list[rune("c"[0])]
		assert.True(t, ok)
	})
	t.Run("finds 4 disctinct answers for \"abdacbcac\"", func(t *testing.T) {
		list, err := DistinctAnswers("abdacbcac")
		assert.Nil(t, err)
		assert.Equal(t, 4, len(list))
		_, ok := list[rune("a"[0])]
		assert.True(t, ok)
		_, ok = list[rune("b"[0])]
		assert.True(t, ok)
		_, ok = list[rune("c"[0])]
		assert.True(t, ok)
		_, ok = list[rune("d"[0])]
		assert.True(t, ok)
	})
}

func TestCommonAnswers_No_Error(t *testing.T) {
	t.Run("for the group [\"abc\"]", func(t *testing.T) {
		t.Run("3 distinct answers found", func(t *testing.T) {
			res := CommonAnswers([]string{"abc"})
			assert.Equal(t, 3, res)
		})
	})
	t.Run("for the group [\"a\", \"a\"]", func(t *testing.T) {
		t.Run("1 distinct answer found", func(t *testing.T) {
			res := CommonAnswers([]string{"a", "a"})
			assert.Equal(t, 1, res)
		})
	})
	t.Run("for the group [\"a\", \"b\"]", func(t *testing.T) {
		t.Run("0 distinct answer found", func(t *testing.T) {
			res := CommonAnswers([]string{"a", "b"})
			assert.Equal(t, 0, res)
		})
	})
	t.Run("for the group [\"a\", \"b\", \"c\"]", func(t *testing.T) {
		t.Run("0 distinct answer found", func(t *testing.T) {
			res := CommonAnswers([]string{"a", "b", "c"})
			assert.Equal(t, 0, res)
		})
	})
	t.Run("for the group [\"ab\", \"ac\"]", func(t *testing.T) {
		t.Run("0 distinct answer found", func(t *testing.T) {
			res := CommonAnswers([]string{"ab", "ac"})
			assert.Equal(t, 1, res)
		})
	})
	t.Run("for the group [\"a\", \"a\", \"a\", \"a\"]", func(t *testing.T) {
		t.Run("0 distinct answer found", func(t *testing.T) {
			res := CommonAnswers([]string{"a", "a", "a", "a"})
			assert.Equal(t, 1, res)
		})
	})
}

func TestCommonAnswers_from_Sample(t *testing.T) {
	// t.Run("", func(t *testing.T) {
	// 	res := CommonAnswers([]string{
	// 		"xgedfibnyuhqsrazlwtpocj",
	// 		"fxgpoqijdzybletckwaunsr",
	// 		"pwnqsizrfcbyljexgouatd",
	// 		"ljtperqsodghnufiycxwabz",
	// 	})
	// 	assert.Equal(t, 22, res)
	// })
	// t.Run("", func(t *testing.T) {
	// 	res := CommonAnswers([]string{
	// 		"zqtkcomfdyrs",
	// 		"qokdtmzyawfrlc",
	// 		"qdmzejfctykor",
	// 	})
	// 	assert.Equal(t, 11, res)
	// })
	t.Run("", func(t *testing.T) {
		groups := [][]string{
			{"abc"},
			{"a", "b", "c"},
			{"ab", "ac"},
			{"a", "a", "a", "a"},
			{"b"},
		}
		sum := 0
		for _, g := range groups {
			c := CommonAnswers(g)
			sum += c
		}
		assert.Equal(t, 6, sum)
	})

	t.Run("", func(t *testing.T) {
		res := CommonAnswers([]string{
			"nik",
			"yfi",
			"i",
			"i",
		})
		assert.Equal(t, 1, res)
	})
}
