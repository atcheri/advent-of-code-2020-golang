package main

import (
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/atcheri/advent-of-code-2020-golang/day6/yesno"
)

func readFile(fname string) ([]string, error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	groups := make([]string, 0)
	answers := ""

	isNew := true
	for _, l := range lines {
		if len(l) == 0 {
			if !isNew {
				groups = append(groups, answers)
				isNew = true
				answers = ""
			}
			continue
		}
		answers += l
		isNew = false
	}

	return groups, nil
}

func readFile2(fname string) ([][]string, error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	groups := make([][]string, 0)
	var answers []string

	isNew := true
	for _, l := range lines {
		if len(l) == 0 {
			if !isNew {
				groups = append(groups, answers)
				isNew = true
				answers = answers[:0]
			}
			continue
		}
		answers = append(answers, l)
		isNew = false
	}

	return groups, nil
}

func main() {
	// 1st part
	groupAnswers, err := readFile("./input")
	if err != nil {
		panic("Coundl't read from input file")
	}

	sum := 0
	for _, gas := range groupAnswers {
		answers, err := yesno.DistinctAnswers(gas)
		if err != nil {
			panic("Opps there was an enormous error somewhere")
		}
		sum += len(answers)
	}
	fmt.Printf("The total number of \"yes\" is: %d\n", sum)

	// 2nd part
	sum = 0
	groupAnswersSlice, err := readFile2("./input")
	if err != nil {
		panic("Coundl't read from input file")
	}
	for _, gas := range groupAnswersSlice {
		c := yesno.CommonAnswers(gas)
		sum += c
	}
	fmt.Printf("The total number of \"yes\" is: %d\n", sum)

}
