package main

import (
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/atcheri/advent-of-code-2020-golang/day3/forest"
)

func readFile(fname string) ([]string, error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	forest := make([]string, 0, len(lines))

	for _, l := range lines {
		if len(l) == 0 {
			continue
		}
		forest = append(forest, l)
	}

	return forest, nil
}

func main() {
	forestMap, err := readFile("./input")
	if err != nil {
		panic("Impossible to see the forest")
	}
	treeHits, _ := forest.TreeHits(forestMap, 3, 1)
	fmt.Printf("%d\n", treeHits)

	manySteps := []struct {
		right int
		down  int
	}{
		{right: 1, down: 1},
		{right: 3, down: 1},
		{right: 5, down: 1},
		{right: 7, down: 1},
		{right: 1, down: 2},
	}
	trees := make([]int, 0, len(manySteps))
	for _, steps := range manySteps {
		hits, _ := forest.TreeHits(forestMap, steps.right, steps.down)
		trees = append(trees, hits)
	}
	fmt.Printf("%+v\n", trees)

	fmt.Printf("Product of the hit trees: %d\n", func(ts ...int) int {
		p := 1
		for _, t := range ts {
			p *= t
		}
		return p
	}(trees...))
}
