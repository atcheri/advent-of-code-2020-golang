package forest

import (
	"errors"
)

var (
	step                      = rune("."[0])
	tree                      = rune("#"[0])
	invalidForestErrorMsg     = "Invalid forest error"
	invalidForestLineErrorMsg = "Invalid forest line error"
	emptyForestErrorMsg       = "Empty forest error"
)

func TreeHits(forest []string, steps int, down int) (int, error) {
	if len(forest) == 0 {
		return 0, errors.New(emptyForestErrorMsg)
	}
	if err := validateForest(forest); err != nil {
		return 0, errors.New(invalidForestErrorMsg)
	}

	count, curr := 0, 0
	var filterForest = func(forest []string, down int) []string {
		if down == 1 {
			return forest
		}
		var tmp []string
		for i, l := range forest {
			if i%down == 0 {
				tmp = append(tmp, l)
			}
		}
		return tmp
	}

	cleanForest := filterForest(forest, down)
	for il, line := range cleanForest {
		if il >= len(cleanForest)-1 {
			break
		}
		nextLine := cleanForest[il+1]
		curr += steps
		width := len(line)
		ind := curr % width
		if rune(nextLine[ind]) == tree {
			count++
		}
	}

	return count, nil
}

func validateForest(forest []string) error {
	for _, line := range forest {
		err := validateForestLine(line)
		if err != nil {
			return err
		}
	}
	return nil
}

func validateForestLine(line string) error {
	for _, c := range line {
		if c != step && c != tree {
			return errors.New(invalidForestLineErrorMsg)
		}
	}
	return nil
}
