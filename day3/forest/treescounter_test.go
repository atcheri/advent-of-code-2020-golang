package forest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateForstLine(t *testing.T) {
	t.Run("[ERROR] invalid line", func(t *testing.T) {
		err := validateForestLine("51848qdr")
		assert.EqualError(t, err, invalidForestLineErrorMsg)
	})
	t.Run("[NO ERROR] valid line", func(t *testing.T) {
		err := validateForestLine("..#..##")
		assert.Nil(t, err)
	})
}

// func TestIsTree_Error(t *testing.T) {
// 	t.Run("", func(t *testing.T) {

// 	})
// }

func TestTreeHits_Error(t *testing.T) {
	t.Run("[ERROR]", func(t *testing.T) {
		t.Run("empty forest", func(t *testing.T) {
			_, err := TreeHits([]string{}, 0, 1)
			assert.EqualError(t, err, emptyForestErrorMsg)
		})
		t.Run("invalid forest format", func(t *testing.T) {
			_, err := TreeHits([]string{"...#..", "frbqfg"}, 0, 1)
			assert.EqualError(t, err, invalidForestErrorMsg)
		})
	})
}

func TestTreeHits_NO_Error(t *testing.T) {
	t.Run("[NO ERROR]", func(t *testing.T) {
		t.Run("From the example with 3 steps", func(t *testing.T) {
			forest := []string{
				"..##.......",
				"#...#...#..",
				".#....#..#.",
				"..#.#...#.#",
				".#...##..#.",
				"..#.##.....",
				".#.#.#....#",
				".#........#",
				"#.##...#...",
				"#...##....#",
				".#..#...#.#",
			}
			count, err := TreeHits(forest, 3, 1)
			assert.Nil(t, err)
			assert.Equal(t, 7, count)
		})

	})
}
func TestTreeHits_Second_Solution_NO_Error(t *testing.T) {
	t.Run("From the example with 1 steps and 2 downs", func(t *testing.T) {
		forest := []string{
			"..##.......",
			"#...#...#..",
			".#....#..#.",
			"..#.#...#.#",
			".#...##..#.",
			"..#.##.....",
			".#.#.#....#",
			".#........#",
			"#.##...#...",
			"#...##....#",
			".#..#...#.#",
		}
		count, err := TreeHits(forest, 1, 2)
		assert.Nil(t, err)
		assert.Equal(t, 2, count)
	})
}
