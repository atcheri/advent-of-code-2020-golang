package passwordpolicy

import (
	"errors"
	"strconv"
)

type letter = string

// Policy contains the password policy
type Policy struct {
	Letter letter
	Max    int
	Min    int
}

type PassAndPolicy struct {
	Policy   Policy
	Password string
}

var (
	emptyPasswordErrorMsg    = "Empty password error"
	numberLetterErrorMsg     = "Policy letter cannot be a number error"
	passwordTooShortErrorMsg = "Password too short error"
	minTooHighErrorMsg       = "Minimum is biger than the password length error"
)

func ExtractValidPasswords(input []PassAndPolicy) []string {
	valids := make([]string, 0)
	for _, pp := range input {
		if ok, _ := verifiesPolicy(pp.Password, pp.Policy); ok {
			valids = append(valids, pp.Password)
		}
	}
	return valids
}

func ExtractValidNewPasswords(input []PassAndPolicy) []string {
	valids := make([]string, 0)
	for _, pp := range input {
		if ok, _ := verifiesNewPolicy(pp.Password, pp.Policy); ok {
			valids = append(valids, pp.Password)
		}
	}
	return valids
}

func verifiesPolicy(password string, p Policy) (bool, error) {
	if password == "" {
		return false, errors.New(emptyPasswordErrorMsg)
	}
	if _, err := strconv.Atoi(p.Letter); err == nil {
		return false, errors.New(numberLetterErrorMsg)
	}
	count := 0
	for _, s := range password {
		if s == rune(p.Letter[0]) {
			count++
		}
	}
	return count >= p.Min && count <= p.Max, nil
}

func verifiesNewPolicy(password string, p Policy) (bool, error) {
	if password == "" {
		return false, errors.New(emptyPasswordErrorMsg)
	}
	if _, err := strconv.Atoi(p.Letter); err == nil {
		return false, errors.New(numberLetterErrorMsg)
	}
	if p.Max > len(password) {
		return false, errors.New(passwordTooShortErrorMsg)
	}
	if p.Min > len(password)-1 {
		return false, errors.New(minTooHighErrorMsg)
	}

	foundCount := 0
	if password[p.Min-1] == p.Letter[0] {
		foundCount++
	}
	if password[p.Max-1] == p.Letter[0] {
		foundCount++
	}
	if foundCount == 1 {
		return true, nil
	}
	// for i, s := range password {
	// 	if s == rune(p.Letter[0]) && (i == p.Min-1 || i == p.Max-1) {
	// 		fmt.Println(password)
	// 		return true, nil
	// 	}
	// }
	return false, errors.New("Not found error")
}
