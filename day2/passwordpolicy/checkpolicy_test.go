package passwordpolicy

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVerifiesPolicy_Error(t *testing.T) {
	t.Run("[ERROR] Empty password", func(t *testing.T) {
		policy := Policy{Letter: "a", Min: 1, Max: 3}
		_, err := verifiesPolicy("", policy)
		assert.NotNil(t, err)
	})
	t.Run("[ERROR] Policy::Letter must be a letter (string)", func(t *testing.T) {
		policy := Policy{Letter: "6", Min: 1, Max: 3}
		_, err := verifiesPolicy("", policy)
		assert.NotNil(t, err)
	})
}

func TestVerifiesPolicy_No_Error(t *testing.T) {
	t.Run("[NO ERROR] Do not match policy by Letter", func(t *testing.T) {
		policy := Policy{Letter: "a", Min: 1, Max: 3}
		ok, err := verifiesPolicy("b", policy)
		assert.Nil(t, err)
		assert.False(t, ok)
	})
	t.Run("[NO ERROR] Do not match policy by Min", func(t *testing.T) {
		policy := Policy{Letter: "b", Min: 2, Max: 3}
		ok, err := verifiesPolicy("b", policy)
		assert.Nil(t, err)
		assert.False(t, ok)
	})
	t.Run("[NO ERROR] Do not match policy by Max", func(t *testing.T) {
		policy := Policy{Letter: "b", Min: 2, Max: 3}
		ok, err := verifiesPolicy("bbbbb", policy)
		assert.Nil(t, err)
		assert.False(t, ok)
	})
}

func TestVerifiesNewPolicy_No_Error(t *testing.T) {
	t.Run("[NO ERROR] case 1, \"1-3 a: abcde\" should be valid", func(t *testing.T) {
		policy := Policy{Letter: "a", Min: 1, Max: 3}
		ok, err := verifiesNewPolicy("abcde", policy)
		assert.Nil(t, err)
		assert.True(t, ok)
	})
	t.Run("[NO ERROR] case 1, \"1-3 b: cdefg\" should be NOT valid", func(t *testing.T) {
		policy := Policy{Letter: "b", Min: 1, Max: 3}
		ok, err := verifiesNewPolicy("cdefg", policy)
		assert.NotNil(t, err)
		assert.False(t, ok)
	})
	t.Run("[NO ERROR] case 1, \"2-9 c: ccccccccc\" should be valid", func(t *testing.T) {
		policy := Policy{Letter: "c", Min: 2, Max: 9}
		ok, err := verifiesNewPolicy("ccccccccc", policy)
		assert.NotNil(t, err)
		assert.False(t, ok)
	})

	t.Run("[NO ERROR] case 1, \"4-9 c: acb\" should be NOT valid", func(t *testing.T) {
		policy := Policy{Letter: "c", Min: 4, Max: 2}
		ok, err := verifiesNewPolicy("abc", policy)
		assert.NotNil(t, err)
		assert.Equal(t, err.Error(), minTooHighErrorMsg)
		assert.False(t, ok)
	})

	t.Run("[NO ERROR] case 1, \"4-9 c: acb\" should be NOT valid", func(t *testing.T) {
		policy := Policy{Letter: "c", Min: 1, Max: 9}
		ok, err := verifiesNewPolicy("abc", policy)
		assert.NotNil(t, err)
		assert.Equal(t, err.Error(), passwordTooShortErrorMsg)
		assert.False(t, ok)
	})

	t.Run("[NO ERROR] case 1, \"8-12 g: sgwvdxzhkvndv\" should be NOT valid", func(t *testing.T) {
		policy := Policy{Letter: "g", Min: 8, Max: 12}
		ok, err := verifiesNewPolicy("sgwvdxzhkvndv", policy)
		assert.NotNil(t, err)
		// assert.Equal(t, err.Error(), passwordTooShortErrorMsg)
		assert.False(t, ok)
	})

}
