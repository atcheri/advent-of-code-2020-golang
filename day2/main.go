package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"gitlab.com/atcheri/advent-of-code-2020-golang/day2/passwordpolicy"
)

func readFile(fname string) ([]passwordpolicy.PassAndPolicy, error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	passwordAndPolicies := make([]passwordpolicy.PassAndPolicy, 0, len(lines))
	// passwords := make([]string, 0, len(lines))

	for _, l := range lines {
		if len(l) == 0 {
			continue
		}
		elements := strings.Split(l, ":")

		var extratPolicy = func(p string) passwordpolicy.Policy {
			parts := strings.Split(p, " ")
			counts := parts[0]
			// letter = parts[len(parts)-1]
			letter := parts[1]
			minMax := strings.Split(counts, "-")
			min, err := strconv.Atoi(minMax[0])
			if err != nil {
				panic("cannot extract min")
			}
			max, err := strconv.Atoi(minMax[len(minMax)-1])
			if err != nil {
				panic("cannot extract max")
			}
			return passwordpolicy.Policy{
				Letter: letter,
				Min:    min,
				Max:    max,
			}
		}
		pp := passwordpolicy.PassAndPolicy{
			extratPolicy(elements[0]),
			strings.ReplaceAll(elements[len(elements)-1], " ", ""),
		}

		passwordAndPolicies = append(passwordAndPolicies, pp)
	}

	return passwordAndPolicies, nil
}

func main() {
	passwordAndPolicies, err := readFile("./input")
	if err != nil {
		panic("Coundl't read from input file")
	}
	// for _, pp := range passwordAndPolicies {
	// 	fmt.Printf("%+v\n", pp)
	// }

	validPasswords := passwordpolicy.ExtractValidPasswords(passwordAndPolicies)
	// for _, v := range validPasswords {
	// 	fmt.Printf("%+v\n", v)
	// }
	fmt.Printf("The number of valid password is: %d\n", len(validPasswords))

	validPasswords = passwordpolicy.ExtractValidNewPasswords(passwordAndPolicies)
	fmt.Printf("The number of valid password is: %d\n", len(validPasswords))

}
