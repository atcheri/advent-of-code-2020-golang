package adapter

import "sort"

// Differences returns a map containing the count of each differences found from the list
func Differences(list []int) map[int]int {

	if len(list) < 1 {
		return nil
	}
	maxDiff := 3
	distrib := make(map[int]int)
	var diff int
	// prev := list[0]
	for i, vi := range list {
		for _, vj := range list[i+1:] {
			diff = vj - vi
			if diff > maxDiff {
				break
			}
			if _, ok := distrib[diff]; ok == false {
				distrib[diff] = 1
			} else {
				distrib[diff]++
			}
		}
	}
	return distrib
}

// Plug prepend 0 and append max + 3
func Plug(list []int) []int {
	if len(list) < 1 {
		return nil
	}
	list = append(list, 0)
	sort.Ints(list)
	list = append(list, list[len(list)-1]+3)
	return list
}

func FindCombinations(list []int, maxDiff int) [][]int {
	if len(list) < 4 {
		return nil
	}
	i, j, diff := 0, 0, 0
	combinations := make([][]int, 0)
	subset := make([]int, 0)
	for {
		j = i
		subset = list[j : j+1]
		for {
			diff = list[j+1] - list[j]
			if diff >= 3 {
				if len(subset) > 2 {
					combinations = append(combinations, subset)
				}
				i = j
				break
			}
			if diff == 1 {
				subset = append(subset, list[j+1])
			}
			if j >= len(list) {
				break
			}
			j++
		}
		i++
		if i >= len(list)-1 {
			break
		}
	}
	return combinations
}

func CountCombinations(set []int, maxDiff int) int {
	distrib := Differences(set)
	if len(set) == 3 {
		return 2
	}
	if len(set) == 4 {
		return 4
	}
	prod := 1
	for i := 2; i <= maxDiff; i++ {
		if v, ok := distrib[i]; ok != false {
			prod *= v
		}
	}
	return prod + 1
}

func Execute(list []int, maxDiff int, countFn func(set []int, maxxDiff int) int, findFn func(list []int, maxDiff int) [][]int) int {
	prod := 1
	for _, combination := range findFn(list, maxDiff) {
		prod *= countFn(combination, maxDiff)
	}

	return prod

}
