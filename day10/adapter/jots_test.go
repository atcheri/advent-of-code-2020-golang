package adapter

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDifferences_No_Error(t *testing.T) {
	t.Run("Given a set of [1, 2, 3, 4]", func(t *testing.T) {
		diffs := Differences([]int{1, 2, 3, 4})
		assert.NotNil(t, diffs)
		assert.EqualValues(t, map[int]int{1: 3, 2: 2, 3: 1}, diffs)
	})

	t.Run("Given a set of [1, 2, 5, 6, 9, 12, 13]", func(t *testing.T) {
		diffs := Differences([]int{1, 2, 5, 6, 9, 12, 13})
		assert.NotNil(t, diffs)
		assert.EqualValues(t, map[int]int{1: 3, 3: 3}, diffs)
	})
}

func Test_From_Example(t *testing.T) {
	t.Run("Finds 7 differences of 1 and 5 of 3", func(t *testing.T) {
		list := []int{16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4}
		diffs := Differences(Plug(list))
		// sorted list 0, 1, 4, 5, 6, 7, 10, 11, 12, 15, 16, 19, 22
		assert.NotNil(t, diffs)
		assert.EqualValues(t, map[int]int{1: 7, 2: 3, 3: 6}, diffs)
	})
	t.Run("Finds 22 differences of 1 and 10 of 3", func(t *testing.T) {
		list := []int{28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3}
		diffs := Differences(Plug(list))
		// sorted: [0, 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31, 32, 33, 34, 35, 38, 39, 42, 45, 46, 47, 48, 49, 52]
		assert.NotNil(t, diffs)
		assert.EqualValues(t, map[int]int{1: 22, 2: 15, 3: 19}, diffs)
	})
}

func TestFindCombinations(t *testing.T) {
	maxDiff := 3
	t.Run("Returns an empty list", func(t *testing.T) {
		list := []int{1}
		combinations := FindCombinations(Plug(list), maxDiff)
		assert.Equal(t, 0, len(combinations))
	})
	t.Run("Returns a list with one set [1, 2, 3]", func(t *testing.T) {
		list := []int{1, 2, 3}
		combinations := FindCombinations(Plug(list), maxDiff)
		assert.Equal(t, 1, len(combinations))
		assert.EqualValues(t, [][]int{{0, 1, 2, 3}}, combinations)
	})
	t.Run("Returns a list with one set [1, 2, 3, 4]", func(t *testing.T) {
		list := []int{1, 2, 3, 4}
		combinations := FindCombinations(Plug(list), maxDiff)
		assert.Equal(t, 1, len(combinations))
		assert.EqualValues(t, [][]int{{0, 1, 2, 3, 4}}, combinations)
	})
	t.Run("Returns a list with one set [1, 2, 3, 6]", func(t *testing.T) {
		list := []int{1, 2, 3, 6}
		combinations := FindCombinations(Plug(list), maxDiff)
		assert.Equal(t, 1, len(combinations))
		assert.EqualValues(t, [][]int{{0, 1, 2, 3}}, combinations)
	})
	t.Run("Returns a list with one set [1, 2, 3, 6, 7, 10, 11, 12, 15, 16]", func(t *testing.T) {
		list := []int{1, 2, 3, 6, 7, 10, 11, 12, 15, 16}
		combinations := FindCombinations(Plug(list), maxDiff)
		assert.Equal(t, 2, len(combinations))
		assert.EqualValues(t, [][]int{{0, 1, 2, 3}, {10, 11, 12}}, combinations)
	})
}

func TestFindCombinations_From_Example(t *testing.T) {
	maxDiff := 3
	t.Run("Returns 2 list of [4, 5, 6, 7] and [10, 11, 12]", func(t *testing.T) {
		list := []int{16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4}
		combinations := FindCombinations(Plug(list), maxDiff)
		assert.Equal(t, 2, len(combinations))
		assert.EqualValues(t, [][]int{
			{4, 5, 6, 7},
			{10, 11, 12},
		}, combinations)
	})
	t.Run("Returns many combinations", func(t *testing.T) {
		list := []int{28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3}
		// sorted : [1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31, 32, 33, 34, 35, 38, 39, 42, 45, 46, 47, 48, 49]
		combinations := FindCombinations(Plug(list), maxDiff)
		assert.Equal(t, 6, len(combinations))
		assert.EqualValues(t, [][]int{
			{0, 1, 2, 3, 4},
			{7, 8, 9, 10, 11},
			{17, 18, 19, 20},
			{23, 24, 25},
			{31, 32, 33, 34, 35},
			{45, 46, 47, 48, 49},
		}, combinations)
	})
}

func TestCountCombinations_From_Example(t *testing.T) {
	maxDiff := 3
	t.Run("Returns 1 for a set of [0, 1, 2]", func(t *testing.T) {
		set := []int{0, 1, 2}
		count := CountCombinations(set, maxDiff)
		assert.Equal(t, 2, count)
	})
	t.Run("Returns 7 for a set of [0, 1, 2, 3]", func(t *testing.T) {
		set := []int{0, 1, 2, 3}
		count := CountCombinations(set, maxDiff)
		assert.Equal(t, 4, count)
	})
	t.Run("Returns 7 for a set of [0, 1, 2, 3, 4]", func(t *testing.T) {
		set := []int{0, 1, 2, 3, 4}
		count := CountCombinations(set, maxDiff)
		assert.Equal(t, 7, count)
	})
	t.Run("Returns 7 for a set of [0, 1, 2, 3, 4, 5]", func(t *testing.T) {
		set := []int{0, 1, 2, 3, 4, 5}
		count := CountCombinations(set, maxDiff)
		assert.Equal(t, 13, count)
	})
}

func TestAll_From_Example(t *testing.T) {
	t.Run("given example list 1", func(t *testing.T) {
		t.Run("Returns 8 combinations", func(t *testing.T) {
			maxDiff := 3
			list := []int{16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4}
			count := Execute(Plug(list), maxDiff, CountCombinations, FindCombinations)
			assert.Equal(t, 8, count)
		})
	})
	t.Run("given example list 2", func(t *testing.T) {
		t.Run("Returns 19208 combinations", func(t *testing.T) {
			maxDiff := 3
			list := []int{28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3}
			count := Execute(Plug(list), maxDiff, CountCombinations, FindCombinations)
			assert.Equal(t, 19208, count)
		})
	})
}
