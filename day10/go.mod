module gitlab.com/atcheri/advent-of-code-2020-golang/day10

go 1.15

require (
	github.com/adam-lavrik/go-imath v0.0.0-20200104135348-19d2b923d06c
	github.com/stretchr/testify v1.6.1
)
