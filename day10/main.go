package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"gitlab.com/atcheri/advent-of-code-2020-golang/day10/adapter"
)

func readFile(fname string) (nums []int, err error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	nums = make([]int, 0, len(lines))

	for _, l := range lines {
		if len(l) == 0 {
			continue
		}
		n, err := strconv.Atoi(l)
		if err != nil {
			return nil, err
		}
		nums = append(nums, n)
	}

	return nums, nil
}

func main() {
	entries, err := readFile("./input")
	if err != nil {
		panic("Coundl't read from input file")
	}
	mock := make([]int, len(entries))

	copy(mock, entries)
	diffs := adapter.Differences(adapter.Plug(mock))
	fmt.Printf("%v\n", diffs)
	fmt.Printf("Solution n°1: %v\n", diffs[1]*diffs[3])

	// part 2
	joltages := adapter.Plug(mock)

	fmt.Printf("Solution n° 2: The maximum number of distinct combinations is: %d\n", adapter.Execute(joltages, 3, adapter.CountCombinations, adapter.FindCombinations))
}
