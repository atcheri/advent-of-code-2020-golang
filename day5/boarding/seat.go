package boarding

import (
	"errors"
	"regexp"
	"sync"
)

var (
	codePrefixRegexp               = "[^][FB]"
	codeSuffixRegexp               = "[^][RL]"
	codeLength                     = 10
	codePrefixLength               = 7
	codeSuffixLength               = 3
	invalidCodeLengthErrorMsg      = "Code length must be equal to 10 error"
	invalidCharacterPrefixErrorMsg = "Invalid character in code prefix error"
	invalidCharacterSuffixErrorMsg = "Invalid character in code suffix error"
	invalidRegexpErrorMsg          = "Invalid regexp (string parameter) error"
	maxRows                        = 128 - 1
	maxSeats                       = 8 - 1
)

type Seat struct {
	code string
}

func (s *Seat) FindId() int {
	var wg sync.WaitGroup
	row, col := 0, 0
	wg.Add(2)

	go func() {
		row = converge(s.code[0:7], struct {
			high string
			low  string
		}{
			high: "B",
			low:  "F",
		}, 0, maxRows)
		wg.Done()
	}()
	go func() {
		col = converge(s.code[7:], struct {
			high string
			low  string
		}{
			high: "R",
			low:  "L",
		}, 0, maxSeats)
		wg.Done()
	}()

	wg.Wait()

	return row*8 + col
}

func converge(seq string, highLow struct {
	high string
	low  string
}, min int, max int) int {
	mid := min + (max-min)/2
	for _, s := range seq[0 : len(seq)-1] {
		mid = min + (max-min)/2
		if s == rune(highLow.high[0]) {
			min = mid + 1
		} else {
			max = mid
		}
	}

	// return ix.Min(min, max)
	if seq[len(seq)-1] == byte(highLow.high[0]) {
		return max
	}
	return min
}

func validate(code string) error {
	if len(code) != codeLength {
		return errors.New(invalidCodeLengthErrorMsg)
	}

	// checking prefix, the 7 first characters
	re, err := regexp.Compile(codePrefixRegexp)
	if err != nil {
		return errors.New(invalidRegexpErrorMsg)
	}
	invalidPrefix := re.Match([]byte(code[:7]))
	if invalidPrefix {
		return errors.New(invalidCharacterPrefixErrorMsg)
	}

	// checking suffix, the 3 last characters
	re, err = regexp.Compile(codeSuffixRegexp)
	if err != nil {
		return errors.New(invalidRegexpErrorMsg)
	}
	invalidSuffix := re.Match([]byte(code[7:]))
	if invalidSuffix {
		return errors.New(invalidCharacterSuffixErrorMsg)
	}
	return nil
}

func Scan(code string) (*Seat, error) {
	if err := validate(code); err != nil {
		return nil, err
	}
	seat := &Seat{code: code}
	return seat, nil
}
