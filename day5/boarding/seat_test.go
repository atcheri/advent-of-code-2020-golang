package boarding

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidate_Error(t *testing.T) {
	t.Run("Invalid code length", func(t *testing.T) {
		code := "ABC"
		err := validate(code)
		assert.NotNil(t, err)
		assert.EqualError(t, err, invalidCodeLengthErrorMsg)
	})
	t.Run("Invalid character (A) in code", func(t *testing.T) {
		code := "AAAAAAAAAA"
		err := validate(code)
		assert.NotNil(t, err)
		assert.EqualError(t, err, invalidCharacterPrefixErrorMsg)
	})
	t.Run("Invalid prefix in code with valid suffix", func(t *testing.T) {
		code := "AAAAAAARLR"
		err := validate(code)
		assert.NotNil(t, err)
		assert.EqualError(t, err, invalidCharacterPrefixErrorMsg)
	})
	t.Run("Invalid suffix in code with valid prefix", func(t *testing.T) {
		code := "FBFBBFFAAA"
		err := validate(code)
		assert.NotNil(t, err)
		assert.EqualError(t, err, invalidCharacterSuffixErrorMsg)
	})
}

func TestValidate_No_Error(t *testing.T) {
	t.Run("Valid code: FBFBBFFRLR", func(t *testing.T) {
		code := "FBFBBFFRLR"
		err := validate(code)
		assert.Nil(t, err)
	})
}

func TestConverge(t *testing.T) {
	t.Run("Returns 0 for the sequence \"F\"", func(t *testing.T) {
		res := converge("F", struct {
			high string
			low  string
		}{
			high: "B",
			low:  "F",
		}, 0, maxRows)
		assert.Equal(t, 0, res)
	})
	t.Run("Returns 128 for the sequence \"B\"", func(t *testing.T) {
		res := converge("B", struct {
			high string
			low  string
		}{
			high: "B",
			low:  "F",
		}, 0, maxRows)
		assert.Equal(t, maxRows, res)
	})
	// t.Run("Returns 0 for the sequence \"FB\"", func(t *testing.T) {
	// 	res := converge("FB", struct {
	// 		high string
	// 		low  string
	// 	}{
	// 		high: "B",
	// 		low:  "F",
	// 	}, 0, maxRows)
	// 	assert.Equal(t, 64, res)
	// })
	t.Run("Returns 64 for the sequence \"B\"", func(t *testing.T) {
		res := converge("BF", struct {
			high string
			low  string
		}{
			high: "B",
			low:  "F",
		}, 0, maxRows)
		assert.Equal(t, 64, res)
	})

	t.Run("Returns 44 for the sequence \"FBFBBFF\"", func(t *testing.T) {
		res := converge("FBFBBFF", struct {
			high string
			low  string
		}{
			high: "B",
			low:  "F",
		}, 0, maxRows)
		assert.Equal(t, 44, res)
	})
	t.Run("Returns 5 for the sequence \"RLR\"", func(t *testing.T) {
		res := converge("RLR", struct {
			high string
			low  string
		}{
			high: "R",
			low:  "L",
		}, 0, maxSeats)
		assert.Equal(t, 5, res)
	})
}

func TestConverge_Sample(t *testing.T) {
	t.Run("Returns row 70 and column 7 for the sequence \"BFFFBBFRRR\"", func(t *testing.T) {
		res := converge("BFFFBBF", struct {
			high string
			low  string
		}{
			high: "B",
			low:  "F",
		}, 0, maxRows)
		assert.Equal(t, 70, res)

		res = converge("RRR", struct {
			high string
			low  string
		}{
			high: "R",
			low:  "L",
		}, 0, maxSeats)
		assert.Equal(t, 7, res)
	})
	t.Run("Returns row 70 and column 7 for the sequence \"FFFBBBFRRR\"", func(t *testing.T) {
		res := converge("FFFBBBF", struct {
			high string
			low  string
		}{
			high: "B",
			low:  "F",
		}, 0, maxRows)
		assert.Equal(t, 14, res)

		res = converge("RRR", struct {
			high string
			low  string
		}{
			high: "R",
			low:  "L",
		}, 0, maxSeats)
		assert.Equal(t, 7, res)
	})
	t.Run("Returns row 70 and column 7 for the sequence \"BBFFBBFRLL\"", func(t *testing.T) {
		res := converge("BBFFBBF", struct {
			high string
			low  string
		}{
			high: "B",
			low:  "F",
		}, 0, maxRows)
		assert.Equal(t, 102, res)

		res = converge("RLL", struct {
			high string
			low  string
		}{
			high: "R",
			low:  "L",
		}, 0, maxSeats)
		assert.Equal(t, 4, res)
	})
}

func TestFindId_Error(t *testing.T) {
	t.Run("Returns id=357 for sequence \"FBFBBFFRLR\"", func(t *testing.T) {
		seat := &Seat{code: "FBFBBFFRLR"}
		id := seat.FindId()
		assert.Equal(t, 357, id)
	})
	t.Run("Returns id=567 for sequence \"BFFFBBFRRR\"", func(t *testing.T) {
		seat := &Seat{code: "BFFFBBFRRR"}
		id := seat.FindId()
		assert.Equal(t, 567, id)
	})
	t.Run("Returns id=119 for sequence \"FFFBBBFRRR\"", func(t *testing.T) {
		seat := &Seat{code: "FFFBBBFRRR"}
		id := seat.FindId()
		assert.Equal(t, 119, id)
	})
	t.Run("Returns id=820 for sequence \"BBFFBBFRLL\"", func(t *testing.T) {
		seat := &Seat{code: "BBFFBBFRLL"}
		id := seat.FindId()
		assert.Equal(t, 820, id)
	})
}
