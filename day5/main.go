package main

import (
	"fmt"
	"io/ioutil"
	"strings"
	"sync"

	"github.com/adam-lavrik/go-imath/ix"

	"gitlab.com/atcheri/advent-of-code-2020-golang/day5/boarding"
)

func readFile(fname string) (codes []string, err error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	codes = make([]string, 0, len(lines))

	for _, l := range lines {
		if len(l) == 0 {
			continue
		}
		if err != nil {
			return nil, err
		}
		codes = append(codes, l)
	}

	return codes, nil
}

func fanIn(
	done <-chan interface{},
	channels ...<-chan int,
) <-chan int {
	var wg sync.WaitGroup
	multiPlexedStream := make(chan int)

	multiplex := func(c <-chan int) {
		defer wg.Done()
		for i := range c {
			select {
			case <-done:
				return
			case multiPlexedStream <- i:
			}
		}
	}

	// select from all channels
	wg.Add(len(channels))
	for _, c := range channels {
		go multiplex(c)
	}

	// wait for all the calculations to finish
	go func() {
		wg.Wait()
		close(multiPlexedStream)
	}()

	return multiPlexedStream
}

func main() {
	codes, err := readFile("./input")
	if err != nil {
		panic("Coundl't read from input file")
	}

	seatIds := make([]int, len(codes))
	for _, code := range codes {
		seat, err := boarding.Scan(code)
		if err != nil {
			panic("there was an error when scanning code")
		}
		id := seat.FindId()
		seatIds = append(seatIds, id)
	}

	fmt.Printf("Maximum value fro the founds seat IDs is: %d", ix.MaxSlice(seatIds))

}
