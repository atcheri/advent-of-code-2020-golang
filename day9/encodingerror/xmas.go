package encodingerror

// IsValid says whether the next input is valid or not
func IsValid(window []int, value int) bool {
	for i, first := range window {
		for _, second := range window[i+1:] {
			if first+second == value {
				return true
			}
		}
	}
	return false
}

// FindWrongNumber finds the number that doens't follow the rules
func FindWrongNumber(input []int, size int) (int, int, bool) {
	var wrongValue int
	var window []int
	for i := 0; i < len(input)-size; i++ {
		window = input[i : i+size]
		wrongValue = input[i+size]

		if !IsValid(window, wrongValue) {
			return wrongValue, i + size, false
		}
	}
	return -1, -1, true
}

// ContiguousNums finds a list of numbers
func ContiguousNums(input []int, target int) (nums []int) {
	i := 0
	for {
		for _, v := range input[i:] {
			nums = append(nums, v)
			if len(nums) < 2 {
				continue
			}
			sum := Sum(nums)
			if sum > target {
				nums = nums[:0]
				break
			}
			if sum == target {
				return nums
			}
		}
		if i >= len(input) {
			break
		}

		nums = nums[:0]
		i++
	}
	return nums
}

// Sum calculates the sum of a slice of int
func Sum(nums []int) (sum int) {
	for _, v := range nums {
		sum += v
	}
	return
}
