package encodingerror

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsValid_No_Error(t *testing.T) {
	t.Run("Given an array [1, 2] and window size 2", func(t *testing.T) {
		t.Run("the next possible valid value can only be 3", func(t *testing.T) {
			window := []int{1, 2}
			assert.False(t, IsValid(window, 2))
			assert.True(t, IsValid(window, 3))
			assert.False(t, IsValid(window, 4))
			assert.False(t, IsValid(window, 5))
		})
	})
	t.Run("Given an array [1, 2, 3] and window size 3", func(t *testing.T) {
		t.Run("the next possible valid value can only be 3, 4 or 5", func(t *testing.T) {
			window := []int{1, 2, 3}
			assert.False(t, IsValid(window, 2))
			assert.True(t, IsValid(window, 3))
			assert.True(t, IsValid(window, 4))
			assert.True(t, IsValid(window, 5))
			assert.False(t, IsValid(window, 6))
			assert.False(t, IsValid(window, 7))
		})
	})
	t.Run("Given an array [3, 1, 2] and window size 3", func(t *testing.T) {
		t.Run("the next possible valid value can only be 3, 4 or 5", func(t *testing.T) {
			window := []int{3, 1, 2}
			assert.False(t, IsValid(window, 2))
			assert.True(t, IsValid(window, 3))
			assert.True(t, IsValid(window, 4))
			assert.True(t, IsValid(window, 5))
			assert.False(t, IsValid(window, 6))
			assert.False(t, IsValid(window, 7))
		})
	})
	t.Run("Given an array [2, 1, 3] and window size 3", func(t *testing.T) {
		t.Run("the next possible valid value can only be 3, 4 or 5", func(t *testing.T) {
			window := []int{2, 1, 3}
			assert.False(t, IsValid(window, 2))
			assert.True(t, IsValid(window, 3))
			assert.True(t, IsValid(window, 4))
			assert.True(t, IsValid(window, 5))
			assert.False(t, IsValid(window, 6))
			assert.False(t, IsValid(window, 7))
		})
	})
	t.Run("Given an array [2, 3, 1] and window size 3", func(t *testing.T) {
		t.Run("the next possible valid value can only be 3, 4 or 5", func(t *testing.T) {
			window := []int{2, 3, 1}
			assert.False(t, IsValid(window, 2))
			assert.True(t, IsValid(window, 3))
			assert.True(t, IsValid(window, 4))
			assert.True(t, IsValid(window, 5))
			assert.False(t, IsValid(window, 6))
			assert.False(t, IsValid(window, 7))
		})
	})
	t.Run("Given an array [2, 2, 1] and window size 3", func(t *testing.T) {
		t.Run("the next possible valid value can only be 3, 4 or 5", func(t *testing.T) {
			window := []int{3, 2, 1}
			assert.False(t, IsValid(window, 2))
			assert.True(t, IsValid(window, 3))
			assert.True(t, IsValid(window, 4))
			assert.True(t, IsValid(window, 5))
			assert.False(t, IsValid(window, 6))
			assert.False(t, IsValid(window, 7))
		})
	})
}

func TestIsValid_From_Example(t *testing.T) {
	t.Run("From the example with size 5", func(t *testing.T) {
		sample := prepareSample(25, 20)
		wrong, _, validSerie := FindWrongNumber(sample, 5)
		assert.False(t, validSerie)
		assert.Equal(t, 10, wrong)
	})
	t.Run("From the example with size 25 and adding 26", func(t *testing.T) {
		sample := prepareSample(25, 20)
		sample = append(sample, 26)
		wrong, _, validSerie := FindWrongNumber(sample, 25)
		assert.True(t, validSerie)
		assert.Equal(t, -1, wrong)
	})
	t.Run("From the example with size 25 and adding 49", func(t *testing.T) {
		sample := prepareSample(25, 20)
		sample = append(sample, 49)
		wrong, _, validSerie := FindWrongNumber(sample, 25)
		assert.True(t, validSerie)
		assert.Equal(t, -1, wrong)
	})
	t.Run("From the example with size 25 and adding 100", func(t *testing.T) {
		sample := prepareSample(25, 20)
		sample = append(sample, 100)
		wrong, _, validSerie := FindWrongNumber(sample, 25)
		assert.False(t, validSerie)
		assert.Equal(t, 100, wrong)
	})
	t.Run("From the example with size 25 and adding 50", func(t *testing.T) {
		sample := prepareSample(25, 20)
		sample = append(sample, 50)
		wrong, _, validSerie := FindWrongNumber(sample, 25)
		assert.False(t, validSerie)
		assert.Equal(t, 50, wrong)
	})
}

func TestContiguousNums_From_Example(t *testing.T) {
	t.Run("From the example with size 3 and adding 6", func(t *testing.T) {
		input := []int{1, 2, 3, 6}
		res, idx, _ := FindWrongNumber(input, 3)
		nums := ContiguousNums(input[0:idx], res)
		assert.EqualValues(t, []int{1, 2, 3}, nums)
	})
	t.Run("From the example with size 3 and adding 9", func(t *testing.T) {
		input := []int{2, 3, 4, 9}
		res, idx, _ := FindWrongNumber(input, 3)
		nums := ContiguousNums(input[0:idx], res)
		assert.EqualValues(t, []int{2, 3, 4}, nums)
	})
	t.Run("From the example with size 3 and adding 6", func(t *testing.T) {
		input := []int{2, 3, 4, 10, 5, 14, 22}
		res, idx, _ := FindWrongNumber(input, 4)
		nums := ContiguousNums(input[0:idx], res)
		assert.EqualValues(t, []int{3, 4, 10, 5}, nums)
	})
	t.Run("From the example with size 25 and adding 100", func(t *testing.T) {
		sample := prepareSample(25, 20)
		sample = append(sample, 100)
		res, idx, _ := FindWrongNumber(sample, 25)
		nums := ContiguousNums(sample[0:idx], res)
		assert.EqualValues(t, []int{9, 10, 11, 12, 13, 14, 15, 16}, nums)
	})

	t.Run("From the example with size 25 and adding 100", func(t *testing.T) {
		sample := []int{20, 1, 25, 2, 22, 6, 7, 21, 9, 24, 4, 23, 3, 8, 5, 15, 13, 20, 18, 11, 17, 14, 19, 12, 10, 16, 100}
		res, idx, _ := FindWrongNumber(sample, 25)
		nums := ContiguousNums(sample[0:idx], res)
		assert.EqualValues(t, []int{}, nums)
	})
}

func TestContiguousNums_From_Sample(t *testing.T) {
	t.Run("From the given sample", func(t *testing.T) {
		sample := []int{35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576}
		res, idx, _ := FindWrongNumber(sample, 5)
		nums := ContiguousNums(sample[0:idx], res)
		assert.EqualValues(t, []int{15, 25, 47, 40}, nums)

	})
}

var prepareSample = func(size int, first int) []int {
	output := make([]int, size)
	output[0] = first
	for i := 1; i < size; i++ {
		if i >= first {
			output[i] = i + 1
		} else {
			output[i] = i
		}
	}
	return output
}
