package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"gitlab.com/atcheri/advent-of-code-2020-golang/day9/encodingerror"
)

func readFile(fname string) (nums []int, err error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	nums = make([]int, 0, len(lines))

	for _, l := range lines {
		if len(l) == 0 {
			continue
		}
		n, err := strconv.Atoi(l)
		if err != nil {
			return nil, err
		}
		nums = append(nums, n)
	}

	return nums, nil
}

func main() {
	entries, err := readFile("./input")
	if err != nil {
		panic("Coundl't read from input file")
	}
	res, idx, validSerie := encodingerror.FindWrongNumber(entries, 25)
	if validSerie {
		fmt.Println("The complete serie is valid")
		return
	}

	fmt.Printf("The first wrong value was %d\n", res)
	fmt.Printf("The first wrong value index is %d\n", idx)

	nums := encodingerror.ContiguousNums(entries[0:idx], res)
	fmt.Printf("The contiguous set of numbers is %+v\n", nums)

}
