package main

import (
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/atcheri/advent-of-code-2020-golang/day11/seating"
)

func readFile(fname string) ([]string, error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	entries := make([]string, len(lines)-1)

	for i, l := range lines {
		if len(l) == 0 {
			continue
		}
		entries[i] = l
	}

	return entries, nil
}

func main() {
	entries, err := readFile("./input")
	if err != nil {
		panic("Coundl't read from input file")
	}

	ferry, err := seating.NewFerry(entries)
	if err != nil {
		panic("Couldn't build ferry")
	}
	ferry.FinishBoarding()
	fmt.Printf("The total occupied seats is: %d\n", ferry.CountOccupiedSeats())
}
