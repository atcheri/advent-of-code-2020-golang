package seating

import (
	"errors"
)

// Seat is an alias for string
type Seat = string

// Ferry is a struct containing the 2D Seat array
type Ferry struct {
	// Changed int
	seats [][]Seat
	round int
}

var (
	// EMPTY seat
	EMPTY = "L"
	// FLOOR nobody seats
	FLOOR = "."
	// OCCUP is an occupied seat
	OCCUP = "#"
	// VALIDENTRIES is a map of only valid entries
	VALIDENTRIES = map[string]string{
		EMPTY: EMPTY,
		FLOOR: FLOOR,
		OCCUP: OCCUP,
	}
)

// NewFerry is a Ferry builder
func NewFerry(lines []string) (*Ferry, error) {
	ferry := &Ferry{}
	ferry.BuildSeats(lines)
	return ferry, nil
}

// CopyFerry makes a copy of a ferry object
func CopyFerry(f Ferry) Ferry {
	clonedSeats := make([][]Seat, 0)
	// copy(clonedSeats, f.seats)
	for _, row := range f.seats {
		clonedSeats = append(clonedSeats, append([]Seat{}, row...))
	}
	return Ferry{seats: clonedSeats}
}

// BuildSeats convers input to a Seat 2D array
func (f *Ferry) BuildSeats(lines []string) (*Ferry, error) {
	seats := make([][]Seat, len(lines))

	var parseLine = func(line string) ([]Seat, error) {
		row := make([]Seat, len(line))
		for i, c := range line {
			v, ok := VALIDENTRIES[string(c)]
			if !ok {
				return nil, errors.New("Invalid entry error")
			}
			row[i] = v

		}
		return row, nil
	}

	for i, l := range lines {
		row, err := parseLine(l)
		if err != nil {
			return nil, err
		}
		seats[i] = row
	}
	f.seats = seats
	return f, nil
}

// Onboard is the main part
func (f *Ferry) Onboard() *Ferry {
	newFerry := CopyFerry(*f)
	for i, row := range f.seats {
		for j, seat := range row {
			if f.round == 0 && seat != FLOOR {
				newFerry.seats[i][j] = OCCUP
				continue
			}
			newFerry.seats[i][j] = f.changeSeatState(i, j)
		}
	}

	f.seats = newFerry.seats
	f.round++

	return f
}

func (f *Ferry) changeSeatState(i, j int) Seat {
	seat := f.seats[i][j]

	// - Otherwise, the seat's state does not change.
	if seat == FLOOR {
		return FLOOR
	}

	// - If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
	if seat == EMPTY && f.countAdjacentOccupiedSeats(i, j) == 0 {
		return OCCUP
	}

	// - If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
	if seat == OCCUP && f.countAdjacentOccupiedSeats(i, j) > 3 {
		return EMPTY
	}
	return seat
}

func (f *Ferry) countAdjacentOccupiedSeats(i, j int) int {
	count := 0
	width := len(f.seats[0]) // row
	length := len(f.seats)

	for x := i - 1; x <= i+1; x++ {

		if x < 0 || x >= length {
			continue
		}
		for y := j - 1; y <= j+1; y++ {
			if y < 0 || y >= width {
				continue
			}
			if x == i && y == j {
				continue
			}
			// TO CHANGE HERE
			if f.seats[x][y] == OCCUP {
				count++
			}
		}
	}
	return count
}

// FinishBoarding loops until the boarding is finished
func (f *Ferry) FinishBoarding() *Ferry {
	limit := 1e3
	i := 0.0
	var prevFerry Ferry
	for {
		prevFerry = CopyFerry(*f)
		f.Onboard()
		if f.round < 2 {
			continue
		}
		if !AreDifferentFerries(*f, prevFerry) || i > limit {
			break
		}
		i++

	}
	return f
}

// CountOccupiedSeats returns the total occupied seats on the ferry
func (f *Ferry) CountOccupiedSeats() int {
	count := 0
	for _, row := range f.seats {
		for _, seat := range row {
			if seat == OCCUP {
				count++
			}
		}
	}
	return count
}

// AreDifferentFerries checks whether the 2 ferries are different
func AreDifferentFerries(f1, f2 Ferry) bool {
	for i, row := range f1.seats {
		for j, seat := range row {
			if seat != f2.seats[i][j] {
				return true
			}
		}
	}
	return false
}
