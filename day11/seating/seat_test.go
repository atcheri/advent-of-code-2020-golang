package seating

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	lines = []string{
		"L.LL.LL.LL",
		"LLLLLLL.LL",
		"L.L.L..L..",
		"LLLL.LL.LL",
		"L.LL.LL.LL",
		"L.LLLLL.LL",
		"..L.L.....",
		"LLLLLLLLLL",
		"L.LLLLLL.L",
		"L.LLLLL.LL",
	}
)

func TestFerryFactory(t *testing.T) {
	t.Run("Returns a small Fery struct with empty seats", func(t *testing.T) {
		ferry, err := NewFerry([]string{
			"L.LL",
			"LLLL",
		})
		assert.Nil(t, err)
		assert.NotNil(t, ferry)
		assert.EqualValues(t, [][]Seat{
			{EMPTY, FLOOR, EMPTY, EMPTY},
			{EMPTY, EMPTY, EMPTY, EMPTY},
		},
			ferry.seats)
	})
	t.Run("Returns a Fery struct with empty seats", func(t *testing.T) {
		ferry, err := NewFerry(lines)
		assert.Nil(t, err)
		assert.NotNil(t, ferry)
		assert.EqualValues(t, [][]Seat{
			{EMPTY, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY},
			{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY},
			{EMPTY, FLOOR, EMPTY, FLOOR, EMPTY, FLOOR, FLOOR, EMPTY, FLOOR, FLOOR},
			{EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY},
			{EMPTY, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY},
			{EMPTY, FLOOR, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY},
			{FLOOR, FLOOR, EMPTY, FLOOR, EMPTY, FLOOR, FLOOR, FLOOR, FLOOR, FLOOR},
			{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
			{EMPTY, FLOOR, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY},
			{EMPTY, FLOOR, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY},
		},
			ferry.seats)
	})
}

func TestFerry_Onboard(t *testing.T) {
	t.Run("On 1st round of on-boarding", func(t *testing.T) {
		t.Run("All seats are occupied", func(t *testing.T) {
			ferry, _ := NewFerry(lines)
			assert.Equal(t, 0, ferry.round)
			ferry.Onboard()
			assert.Equal(t, 1, ferry.round)
			assert.EqualValues(t, [][]Seat{
				{OCCUP, FLOOR, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP},
				{OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP},
				{OCCUP, FLOOR, OCCUP, FLOOR, OCCUP, FLOOR, FLOOR, OCCUP, FLOOR, FLOOR},
				{OCCUP, OCCUP, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP},
				{OCCUP, FLOOR, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP},
				{OCCUP, FLOOR, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP},
				{FLOOR, FLOOR, OCCUP, FLOOR, OCCUP, FLOOR, FLOOR, FLOOR, FLOOR, FLOOR},
				{OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP},
				{OCCUP, FLOOR, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, FLOOR, OCCUP},
				{OCCUP, FLOOR, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP},
			},
				ferry.seats)
		})
	})
	t.Run("On 2nd round of on-boarding", func(t *testing.T) {
		t.Run("All seats are occupied", func(t *testing.T) {
			ferry, _ := NewFerry(lines)
			assert.Equal(t, 0, ferry.round)
			ferry.Onboard()
			ferry.Onboard()
			assert.Equal(t, 2, ferry.round)
			assert.EqualValues(t, [][]Seat{
				{OCCUP, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, OCCUP, FLOOR, OCCUP, OCCUP},
				{OCCUP, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY, OCCUP},
				{EMPTY, FLOOR, EMPTY, FLOOR, EMPTY, FLOOR, FLOOR, EMPTY, FLOOR, FLOOR},
				{OCCUP, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, OCCUP},
				{OCCUP, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY},
				{OCCUP, FLOOR, EMPTY, EMPTY, EMPTY, EMPTY, OCCUP, FLOOR, OCCUP, OCCUP},
				{FLOOR, FLOOR, EMPTY, FLOOR, EMPTY, FLOOR, FLOOR, FLOOR, FLOOR, FLOOR},
				{OCCUP, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, OCCUP},
				{OCCUP, FLOOR, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY},
				{OCCUP, FLOOR, OCCUP, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, OCCUP, OCCUP},
			},
				ferry.seats)
		})
	})
}

func Test_countAdjacentOccupiedSeats(t *testing.T) {
	t.Run("For a one empty seat ferry", func(t *testing.T) {
		ferry, _ := NewFerry([]string{
			"L",
		})
		seat := ferry.changeSeatState(0, 0)
		assert.Equal(t, OCCUP, seat)
	})
	t.Run("For a 2 rows ferry", func(t *testing.T) {
		ferry, _ := NewFerry([]string{
			"L.LL",
			"LLLL",
		})
		ferry.Onboard()
		assert.Equal(t, 1, ferry.round)
		// 1st row
		assert.Equal(t, OCCUP, ferry.changeSeatState(0, 0))
		assert.Equal(t, FLOOR, ferry.changeSeatState(0, 1))
		assert.Equal(t, EMPTY, ferry.changeSeatState(0, 2), "Should be EMPTY on i=0 j=2")
		assert.Equal(t, OCCUP, ferry.changeSeatState(0, 3))
		// // 2nd row
		assert.Equal(t, OCCUP, ferry.changeSeatState(1, 0))
		assert.Equal(t, EMPTY, ferry.changeSeatState(1, 1))
		assert.Equal(t, EMPTY, ferry.changeSeatState(1, 2))
		assert.Equal(t, OCCUP, ferry.changeSeatState(1, 3))
	})
	t.Run("For a 3 rows ferry", func(t *testing.T) {
		ferry, _ := NewFerry([]string{
			"L.LL",
			"LLLL",
			"L.L.",
		})
		ferry.Onboard()
		assert.Equal(t, 1, ferry.round)
		// 1st row
		assert.Equal(t, OCCUP, ferry.changeSeatState(0, 0))
		assert.Equal(t, FLOOR, ferry.changeSeatState(0, 1))
		assert.Equal(t, EMPTY, ferry.changeSeatState(0, 2), "Should be EMPTY on i=0 j=2")
		assert.Equal(t, OCCUP, ferry.changeSeatState(0, 3))
		// // 2nd row
		assert.Equal(t, OCCUP, ferry.changeSeatState(1, 0))
		assert.Equal(t, EMPTY, ferry.changeSeatState(1, 1))
		assert.Equal(t, EMPTY, ferry.changeSeatState(1, 2))
		assert.Equal(t, EMPTY, ferry.changeSeatState(1, 3))
		// // 2nd row
		assert.Equal(t, OCCUP, ferry.changeSeatState(2, 0))
		assert.Equal(t, OCCUP, ferry.changeSeatState(2, 2))
	})
}

func Test_From_Example_FULL(t *testing.T) {
	t.Run("", func(t *testing.T) {
		ferry, _ := NewFerry(lines)
		assert.Equal(t, 0, ferry.round)
		ferry.FinishBoarding()
		assert.EqualValues(t, [][]Seat{
			{OCCUP, FLOOR, OCCUP, EMPTY, FLOOR, EMPTY, OCCUP, FLOOR, OCCUP, OCCUP},
			{OCCUP, EMPTY, EMPTY, EMPTY, OCCUP, EMPTY, EMPTY, FLOOR, EMPTY, OCCUP},
			{EMPTY, FLOOR, OCCUP, FLOOR, EMPTY, FLOOR, FLOOR, OCCUP, FLOOR, FLOOR},
			{OCCUP, EMPTY, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP, FLOOR, EMPTY, OCCUP},
			{OCCUP, FLOOR, OCCUP, EMPTY, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY},
			{OCCUP, FLOOR, OCCUP, EMPTY, OCCUP, EMPTY, OCCUP, FLOOR, OCCUP, OCCUP},
			{FLOOR, FLOOR, EMPTY, FLOOR, EMPTY, FLOOR, FLOOR, FLOOR, FLOOR, FLOOR},
			{OCCUP, EMPTY, OCCUP, EMPTY, OCCUP, OCCUP, EMPTY, OCCUP, EMPTY, OCCUP},
			{OCCUP, FLOOR, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY},
			{OCCUP, FLOOR, OCCUP, EMPTY, OCCUP, EMPTY, OCCUP, FLOOR, OCCUP, OCCUP},
		},
			ferry.seats)
		assert.Equal(t, 5+1, ferry.round)
		assert.Equal(t, 37, ferry.CountOccupiedSeats())
	})
}

func Test_From_Example_Step_By_Step(t *testing.T) {
	t.Run("", func(t *testing.T) {
		ferry, _ := NewFerry([]string{
			"L.LL.LL.LL",
			"LLLLLLL.LL",
			"L.L.L..L..",
			"LLLL.LL.LL",
		})
		assert.Equal(t, 0, ferry.round)
		// 1st Onboard
		ferry.Onboard()
		assert.EqualValues(t, [][]Seat{
			{OCCUP, FLOOR, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP},
			{OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP},
			{OCCUP, FLOOR, OCCUP, FLOOR, OCCUP, FLOOR, FLOOR, OCCUP, FLOOR, FLOOR},
			{OCCUP, OCCUP, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP},
		}, ferry.seats)
		// 2nd Onboard
		ferry.Onboard()
		assert.EqualValues(t, [][]Seat{
			{OCCUP, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, OCCUP, FLOOR, OCCUP, OCCUP},
			{OCCUP, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY, OCCUP},
			{EMPTY, FLOOR, EMPTY, FLOOR, EMPTY, FLOOR, FLOOR, EMPTY, FLOOR, FLOOR},
			{OCCUP, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, OCCUP},
			// {OCCUP, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY},
			// {OCCUP, FLOOR, EMPTY, EMPTY, EMPTY, EMPTY, OCCUP, FLOOR, OCCUP, OCCUP},
			// {FLOOR, FLOOR, EMPTY, FLOOR, EMPTY, FLOOR, FLOOR, FLOOR, FLOOR, FLOOR},
			// {OCCUP, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, OCCUP},
			// {OCCUP, FLOOR, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY},
			// {OCCUP, FLOOR, OCCUP, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, OCCUP, OCCUP},
		}, ferry.seats)
		// 3rd Onboard
		ferry.Onboard()
		// assert.EqualValues(t, [][]Seat{
		// 	{OCCUP, FLOOR, OCCUP, EMPTY, FLOOR, EMPTY, OCCUP, FLOOR, OCCUP, OCCUP},
		// 	{OCCUP, EMPTY, EMPTY, EMPTY, OCCUP, EMPTY, EMPTY, FLOOR, EMPTY, OCCUP},
		// 	{EMPTY, FLOOR, OCCUP, FLOOR, EMPTY, FLOOR, FLOOR, OCCUP, FLOOR, FLOOR},
		// 	{OCCUP, EMPTY, OCCUP, OCCUP, FLOOR, OCCUP, OCCUP, FLOOR, EMPTY, OCCUP},
		// 	{OCCUP, FLOOR, OCCUP, EMPTY, FLOOR, EMPTY, EMPTY, FLOOR, EMPTY, EMPTY},
		// 	{OCCUP, FLOOR, OCCUP, EMPTY, OCCUP, EMPTY, OCCUP, FLOOR, OCCUP, OCCUP},
		// 	{FLOOR, FLOOR, EMPTY, FLOOR, EMPTY, FLOOR, FLOOR, FLOOR, FLOOR, FLOOR},
		// 	{OCCUP, EMPTY, OCCUP, EMPTY, OCCUP, OCCUP, EMPTY, OCCUP, EMPTY, OCCUP},
		// 	{OCCUP, FLOOR, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, FLOOR, EMPTY},
		// 	{OCCUP, FLOOR, OCCUP, EMPTY, OCCUP, EMPTY, OCCUP, FLOOR, OCCUP, OCCUP},
		// },
		// 	ferry.seats)
		assert.Equal(t, 5, ferry.round)

	})

}
