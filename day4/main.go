package main

import (
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/atcheri/advent-of-code-2020-golang/day4/northpole"
)

func readFile(fname string) (rawPassports [][]string, err error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	rawPassports = make([][]string, 0)
	p := make([]string, 0)

	isNewPass := true
	for _, l := range lines {
		if len(l) == 0 {
			if !isNewPass {
				rawPassports = append(rawPassports, p)
				isNewPass = true
				p = make([]string, 0)
			}
			continue
		}
		p = append(p, l)
		isNewPass = false
	}

	return rawPassports, nil
}

func main() {
	rawPassports, err := readFile("./input")
	if err != nil {
		panic("Coundl't read from input file")
	}
	count := 0
	for _, rp := range rawPassports {
		passport, err := northpole.CreatePassport(rp)
		if err != nil {
			panic(err.Error())
		}
		fmt.Printf("%+v\n", passport)
		if passport.IsValid() {
			count++
		}
	}
	fmt.Printf("The number of valid passports from the sample is: %d\n", count)
}
