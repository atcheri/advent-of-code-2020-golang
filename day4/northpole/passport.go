package northpole

import (
	"errors"
	"strings"
)

// Passport contains all informatio needed to travel
type Passport struct {
	Byr string // (Birth Year)
	Iyr string // (Issue Year)
	Eyr string // (Expiration Year)
	Hgt string // (Height)
	Hcl string // (Hair Color)
	Ecl string // (Eye Color)
	Pid string // (Passport ID)
	Cid string // (Country ID)
}

var (
	emptyEntryErrorMsg    = "No entries to the factory function error"
	invalidFieldsErrorMsg = "Invalid fields error"
)

func (p *Passport) IsValid() bool {
	if p.Hgt == "" || p.Byr == "" || p.Ecl == "" || p.Eyr == "" || p.Hcl == "" || p.Iyr == "" || p.Pid == "" {
		return false
	}
	return true
}

func CreatePassport(lines []string) (*Passport, error) {
	if len(lines) == 0 {
		return nil, errors.New(emptyEntryErrorMsg)
	}
	passport := &Passport{}
	for _, l := range lines {
		groups := strings.Fields(l)
		for _, group := range groups {
			pair := strings.Split(group, ":")
			if len(pair) != 2 {
				return nil, errors.New(invalidFieldsErrorMsg)
			}
			key, value := pair[0], pair[1]
			switch key {
			case "byr":
				passport.Byr = value
			case "iyr":
				passport.Iyr = value
			case "eyr":
				passport.Eyr = value
			case "hgt":
				passport.Hgt = value
			case "hcl":
				passport.Hcl = value
			case "ecl":
				passport.Ecl = value
			case "pid":
				passport.Pid = value
			case "cid":
				passport.Cid = value
			}
		}
	}
	return passport, nil
}
