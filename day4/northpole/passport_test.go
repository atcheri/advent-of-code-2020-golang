package northpole

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreatePassport_Error(t *testing.T) {
	t.Run("No lines", func(t *testing.T) {
		_, err := CreatePassport([]string{})
		assert.NotNil(t, err)
		assert.EqualError(t, err, emptyEntryErrorMsg)
	})
	t.Run("Invalid fields 1 line", func(t *testing.T) {
		_, err := CreatePassport([]string{"abc"})
		assert.NotNil(t, err)
		assert.EqualError(t, err, invalidFieldsErrorMsg)
	})
	t.Run("Invalid fields multiple lines", func(t *testing.T) {
		_, err := CreatePassport([]string{"abc:5 tre:76", "ert:vzzz test_invalid"})
		assert.NotNil(t, err)
		assert.EqualError(t, err, invalidFieldsErrorMsg)
	})
}

func TestCreatePassport_No_Error(t *testing.T) {
	t.Run("No lines", func(t *testing.T) {
		pass, err := CreatePassport([]string{
			"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd",
			"byr:1937 iyr:2017 cid:147 hgt:183cm",
		})
		assert.Nil(t, err)
		assert.Equal(t, "1937", pass.Byr)
		assert.Equal(t, "2017", pass.Iyr)
		assert.Equal(t, "2020", pass.Eyr)
		assert.Equal(t, "183cm", pass.Hgt)
		assert.Equal(t, "#fffffd", pass.Hcl)
		assert.Equal(t, "gry", pass.Ecl)
		assert.Equal(t, "860033327", pass.Pid)
		assert.Equal(t, "147", pass.Cid)
	})
}
func TestPassport_IsValid(t *testing.T) {
	t.Run("Invalid passport", func(t *testing.T) {
		p := Passport{
			Ecl: "gry",
			Pid: "860033327",
			Eyr: "2020",
			Hcl: "#fffffd",
			Byr: "1937",
			Iyr: "2017",
			Cid: "147",
			// Hgt: "183",
		}
		assert.False(t, p.IsValid())
	})
	t.Run("Invalid passport", func(t *testing.T) {
		p := Passport{
			Ecl: "gry",
			Pid: "860033327",
			Eyr: "2020",
			Hcl: "#fffffd",
			// Byr: "1937",
			Iyr: "2017",
			// Cid: "147",
			Hgt: "183",
		}
		assert.False(t, p.IsValid())
	})
	t.Run("Valid incomplete passport", func(t *testing.T) {
		p := Passport{
			Ecl: "gry",
			Pid: "860033327",
			Eyr: "2020",
			Hcl: "#fffffd",
			Byr: "1937",
			Iyr: "2017",
			// Cid: "147",
			Hgt: "183",
		}
		assert.True(t, p.IsValid())
	})
	t.Run("Valid complete passport", func(t *testing.T) {
		p := Passport{
			Ecl: "gry",
			Pid: "860033327",
			Eyr: "2020",
			Hcl: "#fffffd",
			Byr: "1937",
			Iyr: "2017",
			Cid: "147",
			Hgt: "183",
		}
		assert.True(t, p.IsValid())
	})
}

func TestAll_From_Sample(t *testing.T) {
	input := [][]string{
		{
			"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd",
			"byr:1937 iyr:2017 cid:147 hgt:183cm",
		},
		{
			"iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884",
			"hcl:#cfa07d byr:1929",
		},
		{
			"hcl:#ae17e1 iyr:2013",
			"eyr:2024",
			"ecl:brn pid:760753108 byr:1931",
			"hgt:179cm",
		},
		{
			"hcl:#cfa07d eyr:2025 pid:166559648",
			"iyr:2011 ecl:brn hgt:59in",
		},
	}

	count := 0
	for _, rp := range input {
		passport, _ := CreatePassport(rp)
		fmt.Printf("%+v\n", passport)
		if passport.IsValid() {
			count++
		}
	}
	assert.Equal(t, 2, count)

}
