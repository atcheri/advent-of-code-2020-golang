package main

import (
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/atcheri/advent-of-code-2020-golang/day7/colorbags"
)

func readFile(fname string) ([]string, error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(b), "\n")
	entries := make([]string, len(lines))

	for i, l := range lines {
		if len(l) == 0 {
			continue
		}
		entries[i] = l
	}

	return entries, nil
}

func main() {
	entries, err := readFile("./input")
	if err != nil {
		panic("Coundl't read from input file")
	}

	bags := make(map[string]colorbags.Bag)
	for _, l := range entries {
		if l == "" {
			continue
		}
		bag, err := colorbags.NewBag(l)
		if err != nil {
			panic(fmt.Sprintf("Couldn't create bag, %s", err.Error()))
		}
		bags[bag.ID] = *bag
	}

	db := &colorbags.BagDB{Bags: bags}
	searchedBag := "shiny gold"

	fmt.Printf("The total number of distinct bags is %d\n", len(bags))
	count := db.CountSubBags(&colorbags.BagContainer{ID: searchedBag, Count: -8})
	fmt.Printf("The number of bag that can contain a %s is: %d\n", searchedBag, count)

}
