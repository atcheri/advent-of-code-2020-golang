package colorbags

import (
	"errors"
	"log"
	"regexp"
	"strconv"
	"strings"
)

var (
	wrongParamFormatErroMsg = "Wrong parameter format error"
)

// BagID is a string type alias
type BagID = string

// BagFinder is an interface with a Find function that takes no parameter
type BagFinder interface {
	Find(name string) Bag
}

// BagDB contains a slice of Bag
type BagDB struct {
	Bags map[string]Bag
}

// BagContainer contains bags
type BagContainer struct {
	ID    BagID
	Count int
}

// Bag contains some bag information, like containing parent bags
type Bag struct {
	ID       BagID
	Children []BagContainer
}

// NewBag is the Bag constructor
func NewBag(line string) (*Bag, error) {
	err := validate(line)
	if err != nil {
		return nil, err
	}
	elements := strings.Split(strings.ReplaceAll(line, ".", ""), "contain")
	name := strings.ReplaceAll(elements[0], " bags ", "")

	if strings.Contains(elements[len(elements)-1], "no other") {
		return &Bag{ID: name, Children: nil}, nil
	}

	countReg, err := regexp.Compile("[^1-9]+")
	if err != nil {
		log.Fatal(err)
	}
	reg, err := regexp.Compile("[^a-zA-Z ]|(bags|bag)+")
	if err != nil {
		log.Fatal(err)
	}

	childrenRawBags := strings.Split(elements[len(elements)-1], ", ")
	children := make([]BagContainer, 0, len(childrenRawBags))
	var sanitizedChild string
	for _, c := range childrenRawBags {
		count, err := strconv.Atoi(strings.TrimSpace(countReg.ReplaceAllString(c, "")))
		if err != nil {
			return nil, err
		}
		sanitizedChild = strings.TrimSpace(reg.ReplaceAllString(c, ""))
		child := &BagContainer{
			ID:    sanitizedChild,
			Count: count,
		}
		children = append(children, *child)
	}
	return &Bag{ID: name, Children: children}, nil
}

func validate(line string) error {
	elements := strings.Split(strings.ReplaceAll(line, ".", ""), "contain")
	if len(elements) != 2 {
		return errors.New(wrongParamFormatErroMsg)
	}
	return nil
}

// Find finds
func (db *BagDB) Find(name string) *Bag {
	val, ok := db.Bags[name]
	if !ok {
		return nil
	}
	return &val
}

// Contains has
func (db *BagDB) Contains(name string, root *Bag) bool {
	if root == nil {
		return false
	}
	if root.ID == name {
		return true
	}
	for _, c := range root.Children {
		if db.Contains(name, db.Find(c.ID)) {
			return true
		}
	}
	return false
}

// CountSubBags is
func (db *BagDB) CountSubBags(root *BagContainer) int {
	children := db.Find(root.ID).Children
	if children == nil {
		return 0
	}
	count := 0
	for _, child := range children {
		count += child.Count * (1 + db.CountSubBags(&child))
	}
	return count
}
