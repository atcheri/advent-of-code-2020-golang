package colorbags

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewBag_Error(t *testing.T) {
	t.Run("Throws an error if the line doesn't have the word \"contain\"", func(t *testing.T) {
		_, err := NewBag("random test sentence")
		assert.NotNil(t, err)
		assert.EqualError(t, err, wrongParamFormatErroMsg)
	})
}
func TestNewBag_No_Error(t *testing.T) {
	t.Run("Given \"dotted gray bags contain no other bags.\"", func(t *testing.T) {
		bag, err := NewBag("dotted gray bags contain no other bags.")
		assert.Nil(t, err)
		assert.EqualValues(t, bag, &Bag{
			ID:       "dotted gray",
			Children: nil,
		})
	})
	t.Run("Given \"vibrant purple bags contain 1 pale cyan bag, 1 dotted lavender bag, 3 striped blue bags, 5 clear magenta bags.\"", func(t *testing.T) {
		bag, err := NewBag("vibrant purple bags contain 1 pale cyan bag, 1 dotted lavender bag, 3 striped blue bags, 5 clear magenta bags.")
		assert.Nil(t, err)
		assert.EqualValues(t, bag, &Bag{
			ID: "vibrant purple",
			Children: []BagContainer{
				{ID: "pale cyan", Count: 1},
				{ID: "dotted lavender", Count: 1},
				{ID: "striped blue", Count: 3},
				{ID: "clear magenta", Count: 5},
			},
		})
	})
}

func TestBagDB_Sample_1(t *testing.T) {
	lines := []string{
		"light red bags contain 1 bright white bag, 2 muted yellow bags.",
		"dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
		"bright white bags contain 1 shiny gold bag.",
		"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
		"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
		"dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
		"vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
		"faded blue bags contain no other bags.",
		"dotted black bags contain no other bags.",
	}
	bags := make(map[string]Bag)
	for _, l := range lines {
		bag, err := NewBag(l)
		if err != nil {
			panic(fmt.Sprintf("Couldn't create bag, %s", err.Error()))
		}
		bags[bag.ID] = *bag
	}
	db := &BagDB{Bags: bags}
	searchedBag := "shiny gold"
	t.Run("Contains method", func(t *testing.T) {
		t.Run("Given the sample input", func(t *testing.T) {
			count := 0
			for _, bag := range bags {
				if bag.ID == searchedBag {
					continue
				}
				if db.Contains(searchedBag, &bag) {
					count++
				}
			}
			assert.Equal(t, 4, count)
		})
	})
	t.Run("CountSubBags", func(t *testing.T) {
		t.Run("Given the sample input", func(t *testing.T) {
			count := db.CountSubBags(&BagContainer{ID: searchedBag, Count: -8})
			assert.Equal(t, 32, count)
		})
	})
}

func TestBagDB_Sample_2(t *testing.T) {
	lines := []string{
		"shiny gold bags contain 2 dark red bags.",
		"dark red bags contain 2 dark orange bags.",
		"dark orange bags contain 2 dark yellow bags.",
		"dark yellow bags contain 2 dark green bags.",
		"dark green bags contain 2 dark blue bags.",
		"dark blue bags contain 2 dark violet bags.",
		"dark violet bags contain no other bags.",
	}
	bags := make(map[string]Bag)
	for _, l := range lines {
		bag, err := NewBag(l)
		if err != nil {
			panic(fmt.Sprintf("Couldn't create bag, %s", err.Error()))
		}
		bags[bag.ID] = *bag
	}
	db := &BagDB{Bags: bags}
	searchedBag := "shiny gold"
	t.Run("CountSubBags", func(t *testing.T) {
		t.Run("Given the sample input", func(t *testing.T) {
			count := db.CountSubBags(&BagContainer{ID: searchedBag, Count: -8})
			assert.Equal(t, 126, count)
		})
	})
}
